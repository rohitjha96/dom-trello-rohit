/* eslint-disable func-names */
/* eslint-disable no-console */
/* eslint-disable space-before-function-paren */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-unused-vars */
/* eslint-disable no-empty */
/* eslint-disable eqeqeq */
/* eslint-disable no-use-before-define */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable dot-notation */
/* eslint-disable function-paren-newline */
/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable arrow-parens */
/* eslint-disable comma-dangle */
/* eslint-disable prefer-template */
/* eslint-disable no-undef */
/* eslint-disable prefer-const */
/* eslint-disable operator-linebreak */
/* eslint-disable quotes */
const apiKey = "f8fd58024b0cb495538a72009478e9b1";
const token =
  "cd01d05e0ddf70aec5e7130b16463fdf99378cb2925a98b91f50b5c623f32e9b";

function fetchTrelloBoardByApiKey() {
  let boardResponseObject = fetch(
    "https://api.trello.com/1/boards/pTo2EBXi?fields=name&lists=open&key=" +
      apiKey +
      "&token=" +
      token +
      ""
  );
  let boardJsonResponseObject = boardResponseObject.then(boardResponse =>
    boardResponse.json()
  );
  return boardJsonResponseObject;
}
function fetchListByBoard(board) {
  let listId = board.lists[0].id;
  let listResponseObject = fetch(
    "https://api.trello.com/1/lists/" +
      listId +
      "/cards?f&fields=name&members=true&member_fields=fullName&key=" +
      apiKey +
      "&token=" +
      token +
      ""
  );
  let listJsonResponseObject = listResponseObject.then(listResponse =>
    listResponse.json()
  );
  return listJsonResponseObject;
}
async function fetchChecklistByCard(cards) {
  let checkListPromises = [];

  for (let card in cards) {
    let cardId = cards[card]["id"];

    checkListPromises[card] = fetch(
      "https://api.trello.com/1/cards/" +
        cardId +
        "/checklists?filter=all&fields=all&key=" +
        apiKey +
        "&token=" +
        token +
        ""
    ).then(r => r.json());
  }
  let checklists = await Promise.all(checkListPromises);

  let filteredChecklists = removeEmptyChecklistFromChecklists(checklists);

  return filteredChecklists;
}
function removeEmptyChecklistFromChecklists(checklists) {
  let filteredChecklists = [];
  for (let checklist in checklists) {
    if (checklists[checklist].length == 0) {
    } else {
      filteredChecklists.push(checklists[checklist]);
    }
  }
  return filteredChecklists;
}
async function fetchCheckitemsByChecklists(checklists) {
  let checkItemResponses = [];

  for (let checklist in checklists) {
    for (let eachChecklist in checklists[checklist]) {
      let checkItemId = checklists[checklist][eachChecklist]["id"];
      checkItemResponses.push(
        fetch(
          "https://api.trello.com/1/checklists/" +
            checkItemId +
            "/checkItems?key=" +
            apiKey +
            "&token=" +
            token +
            ""
        ).then(checkitemsResponse => checkitemsResponse.json())
      );
    }
  }

  let checkitemGroupedByChecklist = await Promise.all(checkItemResponses);

  let checkItems = [].concat(...checkitemGroupedByChecklist);

  return checkItems;
}

async function fetchDetails() {
  let board = await fetchTrelloBoardByApiKey();
  let arrayOfCards = await fetchListByBoard(board);

  let checkLists = await fetchChecklistByCard(arrayOfCards);

  let checkItems = await fetchCheckitemsByChecklists(checkLists);

  return checkItems;
}
function createAndDisplayDomElement(checkItems) {
  for (let checkItem in checkItems) {
    let checkItemState = checkItems[checkItem]["state"];
    let checkItemId = checkItems[checkItem]["id"];
    let checkListId = checkItems[checkItem]["idChecklist"];
    let text = checkItems[checkItem]["name"];
    let checkItemSpan = $("<span ></span");
     checkItemSpan.data("checklistid",checkListId);
    checkItemSpan.data("checklistid", checkListId);
    let p = $("<p></p>");
    let input = $(
      "<input type='checkbox'></input>"
    );
    let Button = $(
      "<button> X</button>"
    );
    Button.data("checkitemid",checkItemId);
    checkItemSpan.append(input);
    checkItemSpan.append(p);
    checkItemSpan.append(Button);
    Button.click(function() {
      deleteCheckitem(this);
    });

    p.text(text);
    if (checkItemState == "complete") {
      p.css("text-decoration", "line-through");
      input.prop("checked", "checked");
    } else {
      p.css("text-decoration", "none");
    }
    input.change(function() {
      updatecheckItem(this);
    });
    $("div").append(checkItemSpan);
  }
}
function displayCheckItem() {
  fetchDetails().then(checkItems => {
    createAndDisplayDomElement(checkItems);
  });
}

function addCheckItem() {
  let checkItemName = $("#add-New-To-Do").val();
  fetch(
    "https://api.trello.com/1/checklists/5d81009f50849b61b9dca1d2/checkItems?name=" +
      checkItemName +
      "&pos=bottom&checked=false&key=" +
      apiKey +
      "&token=" +
      token +
      "",
    { method: "POST" }
  )
    .then(response => response.json())
    .then(newCheckItem => {
      let checkListId = newCheckItem["idChecklist"];
      let checkItemId = newCheckItem["id"];
      let checkItemSpan = $("<span ></span");
      checkItemSpan.data("checklistid",checkListId);
      checkItemSpan.data("checklistid", checkListId);
      let p = $("<p></p>");
      let input = $(
        "<input type='checkbox'></input>"
      );
      let Button = $("<button> X</button>");
      Button.data("checkitemid",checkItemId);
      checkItemSpan.append(input);
      checkItemSpan.append(p);
      checkItemSpan.append(Button);
      p.text($("#add-New-To-Do").val());
      $("div").append(checkItemSpan);
      input.change(function() {
        updatecheckItem(this);
      });
      Button.click(function() {
        deleteCheckitem(this);
      });
    });
}
async function getCardId(checkListId) {
  let cardIdObject = await fetch(
    "https://api.trello.com/1/checklists/" +
      checkListId +
      "/cards?key=" +
      apiKey +
      "&token=" +
      token +
      ""
  ).then(r => r.json());
  let cardId = cardIdObject[0]["id"];
  return cardId;
}
function deleteCheckitem(button) {
  let clickedButton = $(button);
  let checkItemId = clickedButton.data("checkitemid")
  let checkListId =clickedButton.parent().data("checklistid");
  let parentSpan = clickedButton.parent();
  parentSpan.remove();
  fetch(
    "https://api.trello.com/1/checklists/" +
      checkListId +
      "/checkItems/" +
      checkItemId +
      "?key=" +
      apiKey +
      "&token=" +
      token +
      "",
    { method: "DELETE" }
  );
}
function updatecheckItem(referencToInputElement) {
  let inputCheckbox = $(referencToInputElement);


  if (referencToInputElement.checked) {
    $(inputCheckbox)
      .siblings("p")
      .css("text-decoration", "line-through");
    let checkItemId = $(inputCheckbox)
      .siblings("button")
      .data("checkitemid");
      let checkListId =$(inputCheckbox).parent().data("checklistid");
    getCardId(checkListId).then(cardId => {
      fetch(
        "https://api.trello.com/1/cards/" +
          cardId +
          "/checkItem/" +
          checkItemId +
          "?state=complete&key=" +
          apiKey +
          "&token=" +
          token +
          "",
        { method: "PUT" }
      );
    });
  } else {
    $(inputCheckbox)
      .siblings("p")
      .css("text-decoration", "none");
      let checkItemId = $(inputCheckbox)
      .siblings("button")
      .data("checkitemid");
      let checkListId =$(inputCheckbox).parent().data("checklistid");
    
    getCardId(checkListId).then(cardId => {
      fetch(
        "https://api.trello.com/1/cards/" +
          cardId +
          "/checkItem/" +
          checkItemId +
          "?state=incomplete&key=" +
          apiKey +
          "&token=" +
          token +
          "",
        { method: "PUT" }
      );
    });
  }
}
function main() {
  displayCheckItem();

  $("#add-Check-Item").click(function() {
    addCheckItem();
  });
}

main();
